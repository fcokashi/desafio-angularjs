app.controller('HomeController', ['$scope', 'repos','openIssues', function($scope, repos, openIssues) {

  $scope.searchString = '';

  $scope.myFunc = function(text) {
      repos.getAPIrepos(text).then(function(result){
          console.log(result.data.items);
          $scope.repos = result.data.items;
      });  
  };

  $scope.cargarIssues = function(repo) {
    if (repo.showIssues){
      repo.showIssues = false;
    }else{
      repo.showIssues = true
      openIssues.getAPIissues(repo.full_name).then(function(result){
        repo.openIssue = result.data.items;
        console.log(repo.openIssue);
    });  
    }
  };
}]);