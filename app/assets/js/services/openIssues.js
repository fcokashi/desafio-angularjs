app.factory('openIssues', ['$http', function($http) {
  var APIissues = {};
  APIissues.getAPIissues = function(fullName) {
    return $http.get('https://api.github.com/search/issues?q=repo:'+ fullName)
  }
  return APIissues;
}]);