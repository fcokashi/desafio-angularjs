app.factory('repos', ['$http', function($http) {
  var APIrepos = {};
  APIrepos.getAPIrepos = function(searchString) {
    return $http.get('https://api.github.com/search/repositories?q='+ searchString)
  }
  return APIrepos;
}]);